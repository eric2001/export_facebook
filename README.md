# Export_Facebook

## Description
Export_Facebook is a module for [Gallery 3](http://gallery.menalto.com/) which works in conjunction with Guillaume Boudreau's facebook app to allow Gallery 3 users to export their photos to their facebook accounts. The module is also capable of exporting the Gallery URL of the original photo, the title, the description, and any tags (if the tags module is active) along with the photo. Please be aware that only public Gallery photos may be exported.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This installs like any other Gallery 3 module.  Download and extract it into your modules folder.  Afterwards log into the Gallery web interface and enable in under the Admin -> Modules menu.  Once the module is active, you will need to set up Guillaume Boudreau's facebook application.  The application can be found on Facebook's web site [here](http://www.facebook.com/apps/application.php?id=2390304162).  Once you've activated it, it will ask you for the URL of the export_facebook module.  This URL should look like http://www.example.com/gallery3/index.php/export_facebook/export_fb .  Enter the correct URL into the facebook application's web page, and then follow the steps to import your photos into your facebook account. 

## History
**Version 1.0.1:**
> - Export photo titles instead of file names.
> - Released 25 January 2010.
>
> Download: [Version 1.0.1](/uploads/556842bd267bbedaac607556b3221986/export_facebook101.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 20 January 2010.
>
> Download: [Version 1.0.0](/uploads/59762aabe659fc2abed23d0e44e75ee8/export_facebook100.zip)
